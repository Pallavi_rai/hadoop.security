# Hadoop Security

**Security** is the main concern when it comes to handling confidential data. Hadoop is the software framework for storing and processing vast amounts of data. As the adoption of Hadoop increases, the volume of data and the types of data handled by Hadoop deployments have also grown.

### Why Hadoop Security?

The rise of the digital universe and the adoption of Hadoop in almost every sector like businesses, finance, health care, military, education, government, etc., security becomes the major concern. **Hadoop must offer strong capabilities to thwart any attacks on the data it stores and take measures to ensure proper security at all times.** So, there should be a mechanism that ensures Hadoop Security. 

### The Three A’s of Security

Hadoop attains a highly competent security wall by following the same approach as seen in the traditional data management system. These include 3 A’s of security:

•	**Authentication**

•	**Authorization**

•	**Auditing**

#### Authentication 

Authentication is the first phase where the authentication of user’s credentials is done. The credentials like User ID and Password are authenticated. This stage ensures that only valid user is allowed access by matching the entered credentials against the saved credentials on the security database.

#### Authorization

It is the second phase where system determines what data or applications user is allowed to access based on the predesignated access control list.

#### Auditing 

In tis stage track of all user operations is kept which includes what data was accessed, added, changed, and what analysis occurred by the user from the period when he login to the cluster.

**Data Protection:** It refers to the use of techniques like encryption and data masking for preventing sensitive data access by unauthorized users and applications.

#### Types of Hadoop Security

•	Kerberos Security

•	HDFS Encryption

•	HDFS File and Directory Permissions

•	Traffic Encryption















